import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: ['http://localhost:2001', 'https://servertest.miraebiz.co.kr', 'https://clienttest.miraebiz.co.kr'],
    credentials: true,
  });
  app.use(cookieParser());
  await app.listen(2001);
}
bootstrap();
