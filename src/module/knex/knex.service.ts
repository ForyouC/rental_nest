import { Injectable } from '@nestjs/common';
import KnexInit from 'knex';
import {config as dotConfig} from 'dotenv';

dotConfig();

@Injectable()
export class KnexService {
  public knex = KnexInit({
    client: 'mssql',
    connection: {
      host: process.env.DB_HOST,
      port: Number(process.env.DB_PORT),
      database: process.env.DB_DATABASE,
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
    },
    pool: { min: 5, max: 10 },
    acquireConnectionTimeout: 10000
  })
}
