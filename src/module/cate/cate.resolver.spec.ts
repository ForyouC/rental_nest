import { Test, TestingModule } from '@nestjs/testing';
import { CateResolver } from './cate.resolver';

describe('CateResolver', () => {
  let resolver: CateResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CateResolver],
    }).compile();

    resolver = module.get<CateResolver>(CateResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
