import { Field, InputType, ObjectType } from "@nestjs/graphql"
import { YnEnum } from "../graphql/graphql.common"

@InputType()
export class GetCategoryListInput {
  @Field({nullable : true, defaultValue: ""})
  CategoryName: string
  @Field({nullable : true, defaultValue: 0})
  Lv: number
  @Field({nullable : true, defaultValue: ""})
  CategoryLink: string
  @Field({nullable : true, defaultValue: ""})
  PCategoryCode: string
  @Field({nullable : true, defaultValue: 0})
  ServiceNo: number
  @Field({nullable : true, defaultValue: "N"})
  PopularRankingYN: YnEnum
}

@ObjectType()
export class GetCategoryListRetrun {
  @Field()
  CategoryNo: number
  @Field()
  Categorycode: string
  @Field()
  CategoryName: string
  @Field()
  LowLvCategoryCount: number
  @Field()
  Lv: number
  @Field()
  CategoryLink: string
  @Field()
  PopularRanking: number
  @Field()
  PCategoryCode: string
  @Field()
  PCategoryName: string
  @Field()
  Ranking: number
  @Field()
  CategoryType: string
  @Field()
  CategoryTypeName: string
}