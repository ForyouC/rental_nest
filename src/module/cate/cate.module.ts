import { Module } from '@nestjs/common';
import { CommonModule } from '../common/common.module';
import { GraphqlModule } from '../graphql/graphql.module';
import { KnexModule } from '../knex/knex.module';
import { CateResolver } from './cate.resolver';
import { CateService } from './cate.service';

@Module({
  imports: [GraphqlModule, KnexModule, CommonModule],
  providers: [CateResolver, CateService]
})
export class CateModule {}
