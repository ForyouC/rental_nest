import { UseGuards } from '@nestjs/common';
import { Args, Context, Info, Query, Resolver } from '@nestjs/graphql';
import { GraphQLResolveInfo } from 'graphql';
import { Roles } from 'src/guard/roles.decoration';
import { RolesGuard } from 'src/guard/roles.guard';
import { contextType } from '../common/common.types';
import { GetCategoryListInput, GetCategoryListRetrun } from './cate.graphql';
import { CateService } from './cate.service';

@Resolver()
export class CateResolver {
  constructor(
    private cateService: CateService
  ){}

  @UseGuards(RolesGuard)
  @Roles("RC")
  @Query(() => [GetCategoryListRetrun])
  getCategoryList(@Args('GetCategoryListInput') args: GetCategoryListInput, @Info() info: GraphQLResolveInfo, @Context() context: contextType){
    return this.cateService.getCategoryList(args, context, info);
  }
}
