import { Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';
import { GraphQLResolveInfo } from 'graphql';
import { CommonService } from '../common/common.service';
import { contextType } from '../common/common.types';
import { GraphqlService } from '../graphql/graphql.service';
import { KnexService } from '../knex/knex.service';
import { GetCategoryListInput } from './cate.graphql';

@Injectable()
export class CateService {
  constructor(
    private graphqlService: GraphqlService,
    private knexService: KnexService,
    private commonService: CommonService
  ){}
  
  getCategoryList = async (args:GetCategoryListInput, context:contextType, info:GraphQLResolveInfo) => {
    const decode:any = this.commonService.permissionChkReturnDecode(context.req, ["RC"]);

    const params = {
      CategoryName: args.CategoryName,
      Lv: args.Lv,
      CategoryLink: args.CategoryLink,
      PCategoryCode: args.PCategoryCode,
      ServiceNo: args.ServiceNo,
      PopularRankingYN: args.PopularRankingYN,
      WorkerID: decode.AName,
      AccessIP: context.req.headers['x-forwarded-for'] ?? "",
    }

    // console.log(params);
  
    const data = await this.knexService.knex.raw(`
    EXEC RT.dbo.asp_GetCategoryList :CategoryName, :Lv, :CategoryLink,:PCategoryCode, :ServiceNo, :PopularRankingYN, :WorkerID, :AccessIP;
    `, params);
  
    if(data.length < 1){
      throw new ApolloError('Permission Error!');
    }

    return data;
  }
}
