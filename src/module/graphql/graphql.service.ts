import { Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-errors';
import { FieldNode, GraphQLResolveInfo, SelectionNode } from 'graphql';
import { KnexService } from 'src/module/knex/knex.service';
import { SearchEnum, SearchOperatorEnum, SearchType, SearchValueType } from './graphql.common';

@Injectable()
export class GraphqlService {
  constructor(
    private knexService: KnexService
  ){}

  selectionSetDepth = (selections: readonly SelectionNode[]) => {
    let selectFiled:any = {};
    selectFiled.fileds = [];
  
    selections && 
    selections.forEach((selection:FieldNode) => {
      if(selection.selectionSet && selection.selectionSet.selections){
        selectFiled[selection.name.value] = this.selectionSetDepth(selection.selectionSet.selections);
      }else{
        selection.name.value !== "__typename" && 
        selectFiled.fileds.push(selection.name.value);
      }
    });
  
    return selectFiled;
  }
  
  getSelections = (info:GraphQLResolveInfo) => {
    let filedNodes:any = {};
    info.fieldNodes.forEach((obj) => {
      if(obj.selectionSet && obj.selectionSet.selections){
        filedNodes[obj.name.value] = this.selectionSetDepth(obj.selectionSet.selections);
      }
    });
  
    return filedNodes;
  }
  
  getSelectGQL = (info:GraphQLResolveInfo, TableName:string) => {
    let selectFiled:string[] = [];
    info.fieldNodes.forEach((obj) => {
      obj.name.value === TableName &&
      obj.selectionSet.selections.forEach((selection:FieldNode) => {
        selection.name.value === "DataList" && 
        selection.selectionSet.selections.forEach((DataList:FieldNode) => {
          DataList.name.value !== "__typename" && selectFiled.push(DataList.name.value);
        })
      })
    });
  
    return selectFiled;
  }
  
  WhereOperator = (str:string) => {
    const operator:any = {
      like: "like",
      eq: "=",
      gt: ">",
      lt: "<",
      gte: ">=",
      lte: "<=",
      btw: "between",
      not_btw: "not between",
      in: "in",
      not_in: "not in",
    }
  
    return operator[str] ?? "=";
  }
  
  WhereFn = (type:SearchEnum, operator:SearchOperatorEnum) => {
    const not_type:any = {
      and_not: true,
      or_not: true,
    };
  
    const btw_or_in_operator:any = {
      btw: true,
      not_btw: true,
      in: true,
      not_in: true,
    }
  
    if(not_type[type] &&  btw_or_in_operator[operator]) throw new ApolloError('and_not or or_not is not suitable for "in" and "between" type subqueries. You should use "not_in" and "not_btw" on value field', 'NOT_MATCH_TYPE_AND_OPERATION');
  
    const whereType:any = {
      and: "where",
      and_not: "orWhere",
      or: "orWhere",
      or_not: "orWhereNot"
    }
  
    return whereType[type] ?? "where";
  }
  
  WhereValue = (searchValue:SearchValueType) => {
    for(const [key, value] of Object.entries(searchValue)){
      return value;
    }
  }
  
  WhereBuilder = (qb:any, search:SearchType[]) => {
    search?.forEach((element) => {
      if(element.filed && element.operator){
        qb[this.WhereFn(element.type, element.operator)](element.filed, this.WhereOperator(element.operator), this.WhereValue(element.value));
      }
  
      if(element.depth){
        qb.where((in_qb:any) => this.WhereBuilder(in_qb, element.depth));
      }
    });
  }
  
  knexQuery = async (from:string, fileds:string[], defaultOrder:string, args:any, info:GraphQLResolveInfo) => {
    const Count:any = await this.knexService.knex.from(from).where((qb:any) => { this.WhereBuilder(qb, args.search) }).count('* as cnt').first();
  
    const DataList = await this.knexService.knex.select(fileds).from(from).where((qb:any) => { this.WhereBuilder(qb, args.search) }).orderBy(args.option.orderBy?.filed ?? defaultOrder, args.option.orderBy?.direction ?? "desc").offset(args.option.offset).limit(args.option.limit);
    
    return {Count: Count.cnt, DataList: DataList};
  }
}
