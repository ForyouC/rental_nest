import { Module } from '@nestjs/common';
import { KnexModule } from 'src/module/knex/knex.module';
import { GraphqlService } from './graphql.service';

@Module({
  imports: [KnexModule],
  providers: [GraphqlService],
  exports: [GraphqlService]
})
export class GraphqlModule {}
