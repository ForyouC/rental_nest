import { InputType, Field, registerEnumType } from '@nestjs/graphql';

@InputType()
export class SearchValueType {
  @Field({nullable: true})
  str?: string
  @Field({nullable: true})
  int?: number
  @Field(() => [String], {nullable: true})
  strArr?: [string]
  @Field(() => [Number], {nullable: true})
  intArr?: [number]
}

@InputType()
export class SearchType {
  @Field(() => SearchEnum, {nullable: true, defaultValue: "and"})
  type?: SearchEnum
  @Field()
  filed: String
  @Field()
  value: SearchValueType
  @Field(() => SearchOperatorEnum)
  operator: SearchOperatorEnum
  @Field(() => [SearchType], {nullable: true})
  depth?: [SearchType]
}

@InputType()
export class OrderBy {
  @Field({nullable: true})
  filed?: String
  @Field(() => DirectionEnum, {nullable: true, defaultValue: "desc"})
  direction?: DirectionEnum
}

@InputType()
export class OptionType {
  @Field({nullable: true, defaultValue: {direction: "desc"}})
  orderBy?: OrderBy
  @Field({nullable: true, defaultValue: 0})
  offset: Number
  @Field({nullable: true, defaultValue: 20})
  limit: Number
}

@InputType()
export class ArgsType {
  @Field(() => [SearchType], {nullable: true})
  search?: [SearchType]
  @Field({nullable: true, defaultValue: {offset: 0, limit: 20, orderBy: {direction: "desc"}}})
  option?: OptionType
}


export enum SearchEnum {
  And = 'and',
  AndNot = 'and_not',
  Or = 'or',
  OrNot = 'or_not'
}

export enum DirectionEnum {
  Asc = 'asc',
  Desc = 'desc'
}

export enum SearchOperatorEnum {
  Btw = 'btw',
  Eq = 'eq',
  Gt = 'gt',
  Gte = 'gte',
  In = 'in',
  Like = 'like',
  Lt = 'lt',
  Lte = 'lte',
  NotBtw = 'not_btw',
  NotIn = 'not_in'
}

export enum YnEnum {
  N = 'N',
  Y = 'Y'
}

registerEnumType(SearchEnum, {
  name: 'SearchEnum',
});

registerEnumType(DirectionEnum, {
  name: 'DirectionEnum',
});

registerEnumType(SearchOperatorEnum, {
  name: 'SearchOperatorEnum',
});

registerEnumType(YnEnum, {
  name: 'SearchOperatorEnum',
});