import { Module } from '@nestjs/common';
import { AgentModule } from 'src/module/agent/agent.module';
import { PartnerResolver } from './partner.resolver';
import { PartnerService } from './partner.service';
import { KnexModule } from 'src/module/knex/knex.module';
import { GraphqlModule } from '../graphql/graphql.module';
import { CommonModule } from '../common/common.module';

@Module({
  imports: [AgentModule, KnexModule, GraphqlModule, CommonModule],
  providers: [PartnerResolver, PartnerService]
})
export class PartnerModule {}
