import { Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-errors';
import { GraphQLResolveInfo } from 'graphql';
import { AgentService } from 'src/module/agent/agent.service';
import { ArgsType } from 'src/module/graphql/graphql.common';
import { KnexService } from 'src/module/knex/knex.service';
import { contextType } from '../common/common.types';
import { GraphqlService } from '../graphql/graphql.service';

@Injectable()
export class PartnerService {
  constructor(
    private knexService: KnexService,
    private agentService: AgentService,
    private graphqlService: GraphqlService
  ){}
  
  async partner(args: ArgsType, context: contextType, info: GraphQLResolveInfo) {
    const fileds = this.graphqlService.getSelections(info)?.partner?.DataList?.fileds;
    if(!fileds) throw new ApolloError('Selection Filed is incorrect!');

    return await this.graphqlService.knexQuery('Partners', fileds, 'PIdx', args, info);
  }
}
