import {  UseGuards } from '@nestjs/common';
import { Args, Context, Info, Query, Resolver } from '@nestjs/graphql';
import { GraphQLResolveInfo } from 'graphql';
import { ArgsType } from 'src/module/graphql/graphql.common';
import { Roles } from 'src/guard/roles.decoration';
import { RolesGuard } from 'src/guard/roles.guard';
import { PartnerReturn } from './partner.graphql';
import { PartnerService } from './partner.service';
import { contextType } from '../common/common.types';

@Resolver()
export class PartnerResolver {
  constructor(private readonly partnerService: PartnerService) {}

  @UseGuards(RolesGuard)
  @Roles("RC")
  @Query(() => PartnerReturn)
  partner(@Args('args') args: ArgsType, @Info() info: GraphQLResolveInfo, @Context() context: contextType) {
    return this.partnerService.partner(args, context, info);
  }
}
