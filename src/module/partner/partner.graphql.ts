import { Field, ObjectType } from "@nestjs/graphql"

@ObjectType()
export class  Partner {
  @Field({nullable: true})
  PIdx?: number
  @Field({nullable: true})
  CompanyName?: string 
  @Field({nullable: true})
  GroupID?: number
  @Field({nullable: true})
  Parent?: number
  @Field({nullable: true})
  Rank?: string
  @Field({nullable: true})
  Individual?: number
  @Field({nullable: true})
  B_Role?: number
  @Field({nullable: true})
  Lock?: string
  @Field({nullable: true})
  DealRate?: number
  @Field({nullable: true})
  CCenter?: string
  @Field({nullable: true})
  Counsel?: number
  @Field({nullable: true})
  Commission?: number
  @Field({nullable: true})
  RDS?: number
  @Field({nullable: true})
  CeoName?: string
  @Field({nullable: true})
  CeoHp?: string
  @Field({nullable: true})
  CeoEmail?: string
  @Field({nullable: true})
  Cms?: number
  @Field({nullable: true})
  Sms?: number
  @Field({nullable: true})
  RegDate?: Date 
}

@ObjectType()
export class PartnerReturn {
  @Field()
  Count: number
  @Field(() => [Partner])
  DataList:[Partner]
}