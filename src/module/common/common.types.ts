import { JwtPayload } from "jsonwebtoken";
import { Request, Response} from "express";

export interface JwtDecodeType extends JwtPayload {
  P_Result: string;
  AIdx: number;
  AName: string;
  Permsn: string;
  Company: number;
  GroupID: number;
  Cms: number;
  Rank: string;
  Confirm: string;
  iat: number;
}

export type contextType = {
  req: Request,
  res: Response
}