import { Injectable } from '@nestjs/common';
import { join } from 'path';
import { readFileSync } from 'fs';
import { sign, verify } from "jsonwebtoken";
import { ApolloError } from "apollo-server-express";
import { Request } from 'express';
import { get } from 'psl';
import {config as dotConfig} from 'dotenv';
import { contextType, JwtDecodeType } from './common.types';

dotConfig();

@Injectable()
export class CommonService {
  deleteObjectProperties = (obj:any, props:string[]) => {
    for(var i = 0; i < props.length; i++) {
        if(obj.hasOwnProperty(props[i])) {
            delete obj[props[i]];
        }
      }
  };

  GenerateJWT = (obj:any) => {
    const file = join(process.cwd(), 'jwtRS256.key');
    var privateKey = readFileSync(file);
    obj.Confirm = process.env.JWT;
    var token = sign(obj, privateKey, { algorithm: 'RS256' });
    return token;
  }
  
  DecodeJWT = (token:string) => {
    const file = join(process.cwd(), 'jwtRS256.key.pub');
    var publicKey = readFileSync(file);
    const result: JwtDecodeType = verify(token, publicKey, { algorithms: ['RS256']}) as JwtDecodeType;
    return result
  }
  
  CookieJWT = (context:contextType, token:string) => {
    const domain = get(context.req.get('host'));
    context.res.cookie("chk", token, { 
      maxAge: 1 * 24 * 60 * 60 * 1000,
      httpOnly: true,
      path: '/',
      expires: new Date(Date.now() + 1 * 24 * 60 * 60 * 1000),
      domain: `.${domain}`,
      sameSite: 'strict',
      secure: true
    });
  }
  
  permissionChk = (req:Request, Rank:String[]) => {
    let Decode;
    if(process.env.NODE_ENV == "development")
    { 
      if(req.cookies.chk){
        Decode = this.DecodeJWT(req.cookies.chk);
      }else if(req.headers.authorization){
        Decode = this.DecodeJWT(req.headers.authorization);
      }
    }else{
      if(req.cookies.chk){
        Decode = this.DecodeJWT(req.cookies.chk);
      }
    }
  
    if(!Decode || !Decode?.Rank || !Rank.includes(Decode?.Rank)){
      return false;
    }else{
      return true;
    }
  }
  
  
  permissionChkReturnDecode = (req:Request, Rank:String[]) => {
    let Decode;
    if(process.env.NODE_ENV == "development")
    { 
      if(req.cookies.chk){
        Decode = this.DecodeJWT(req.cookies.chk);
      }else if(req.headers.authorization){
        Decode = this.DecodeJWT(req.headers.authorization);
      }
    }else{
      if(req.cookies.chk){
        Decode = this.DecodeJWT(req.cookies.chk);
      }
    }
  
    if(!Decode || !Decode?.Rank || !Rank.includes(Decode?.Rank)){
      return false;
    }else{
      return Decode;
    }
  }
  
  DevDecodeJWT = (req:Request) => {
    if(process.env.NODE_ENV == "development")
    { 
      let Decode;
      if(req.cookies.chk){
        Decode = this.DecodeJWT(req.cookies.chk)
      }else if(req.headers.authorization){
        Decode = this.DecodeJWT(req.headers.authorization)
      }
      if(Decode){
        this.deleteObjectProperties(Decode, ["P_Result", "Confirm", "iat"]);
        return Decode;
      }else{
        throw new ApolloError('Token Error');
      }
  
    }else{
      if(req.cookies.chk){
        let Decode = this.DecodeJWT(req.cookies.chk);
        this.deleteObjectProperties(Decode, ["P_Result", "Confirm", "iat"]);
        return Decode;
      }else{
        throw new ApolloError('Token Error');
      }
    }
  }
}
