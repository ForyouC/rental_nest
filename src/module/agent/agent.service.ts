import { Injectable } from '@nestjs/common';
import { LoginAgentInput } from './agent.graphql';
import { ApolloError } from 'apollo-server-errors';
import { GraphQLResolveInfo } from 'graphql';
import { ArgsType } from 'src/module/graphql/graphql.common';
import { KnexService } from 'src/module/knex/knex.service';
import { GraphqlService } from '../graphql/graphql.service';
import { CommonService } from '../common/common.service';
import { contextType } from '../common/common.types';


@Injectable()
export class AgentService {
  constructor(
    private knexService: KnexService,
    private graphqlService: GraphqlService,
    private commonService: CommonService
  ) {}

  async agent(args: ArgsType, context: contextType, info: GraphQLResolveInfo) {
    const fileds = this.graphqlService.getSelections(info)?.agent?.DataList?.fileds;
    if(!fileds) throw new ApolloError('Selection Filed is incorrect!');
    const Data = await this.graphqlService.knexQuery('vw_AgentList', fileds, 'AIdx', args, info);
    return Data;
  }

  async login(LoginAgentInput: LoginAgentInput, context: contextType){
    const params = {
      id: LoginAgentInput.RID ?? "",
      pw: LoginAgentInput.Pwd ?? "",
      ip: context.req.headers['x-forwarded-for'] ?? "",
    };

    console.log(params);

    const data = await this.knexService.knex.raw(`
      DECLARE	@return_value int,
        @P_Result varchar(8),
        @AIdx int,
        @AName varchar(30),
        @Permsn varchar(1),
        @Company int,
        @GroupID int,
        @Cms tinyint,
        @Rank varchar(2)

      EXEC	@return_value = [dbo].[csp_AgentLogin]
        @Ag_Id = :id,
        @Ag_Pwd = :pw,
        @Ag_Ip = :ip,
        @P_Result = @P_Result OUTPUT,
        @AIdx = @AIdx OUTPUT,
        @AName = @AName OUTPUT,
        @Permsn = @Permsn OUTPUT,
        @Company = @Company OUTPUT,
        @GroupID = @GroupID OUTPUT,
        @Cms = @Cms OUTPUT,
        @Rank = @Rank OUTPUT

        SELECT	@P_Result as N'P_Result',
        @AIdx as N'AIdx',
        @AName as N'AName',
        @Permsn as N'Permsn',
        @Company as N'Company',
        @GroupID as N'GroupID',
        @Cms as N'Cms',
        @Rank as N'Rank'
    `, params);

    if(data[1]?.P_Result && data[1].P_Result == "CMS-0000"){
      var token = this.commonService.GenerateJWT(data[1]);
      this.commonService.CookieJWT(context, token);
      return {permission:data[0], result:data[1]};
    }else{
      throw new ApolloError('ID or Pwd is not correct!');
    }
  }

  userState = (context: contextType) => {
    return this.commonService.DevDecodeJWT(context.req);
  }
}
