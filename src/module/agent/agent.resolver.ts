import { Resolver, Query, Mutation, Args, Int, Info, Context } from '@nestjs/graphql';
import { AgentService } from './agent.service';
import { Agent, AgentReturn, LoginReturn, UserStateReturn } from './agent.graphql';
import { LoginAgentInput } from './agent.graphql';
import { GraphQLResolveInfo } from 'graphql';
import { ArgsType } from 'src/module/graphql/graphql.common';
import { SetMetadata, UseGuards } from '@nestjs/common';
import { RolesGuard } from 'src/guard/roles.guard';
import { Roles } from 'src/guard/roles.decoration';
import { contextType } from '../common/common.types';

@Resolver(() => Agent)
export class AgentResolver {
  constructor(private readonly agentService: AgentService) {}

  @UseGuards(RolesGuard)
  @Roles("RC")
  @Query(() => AgentReturn)
  agent(@Args('args') args: ArgsType, @Info() info: GraphQLResolveInfo, @Context() context: contextType) {
    return this.agentService.agent(args,context,info);
  }

  @UseGuards(RolesGuard)
  @Roles("RC")
  @Query(() => UserStateReturn)
  userState(@Context() context: contextType){
    return this.agentService.userState(context);
  }

  @Mutation(() => LoginReturn, { name: 'agentLogin' })
  login(@Args('loginAgentInput') loginAgentInput: LoginAgentInput, @Info() info: GraphQLResolveInfo, @Context() context: contextType) {
    return this.agentService.login(loginAgentInput, context);
  }
}
