import { ObjectType, Field, InputType } from '@nestjs/graphql';

@ObjectType()
export class Agent {
  @Field({nullable: true})
  AIdx?: number;
  @Field({nullable: true})
  RID?: string;
  @Field({nullable: true})
  AName?: string;
  @Field({nullable: true})
  Pwd?: string;
  @Field({nullable: true})
  RegDate?: string;
}

@ObjectType()
export class AgentReturn {
  @Field()
  Count: number;
  @Field(() => [Agent])
  DataList: [Agent];
}

@InputType()
export class LoginAgentInput{
  @Field()
  RID: string;
  @Field()
  Pwd: string;
}


@ObjectType()
export class LoginReturnPermission {
  @Field()
  Receipt: string;
  @Field()
  Receipt_Excel: boolean
  @Field()
  Account: number
  @Field()
  Account_Excel: boolean
  @Field()
  Partner: boolean
  @Field()
  Notice: number
  @Field()
  Pds: number
  @Field()
  Commission: boolean
  @Field()
  Commission_Excel: boolean
  @Field()
  Sms: boolean
  @Field()
  Counsel: boolean
  @Field()
  Product: boolean
  @Field()
  Authority: Boolean
}

@ObjectType()
export class LoginReturnResult {
  @Field()
  P_Result: string
  @Field()
  AIdx: number
  @Field()
  AName: string
  @Field()
  Permsn: string
  @Field()
  Company: string
  @Field()
  GroupID: string
  @Field()
  Cms: number
  @Field()
  Rank: String
}

@ObjectType()
export class LoginReturn {
  @Field()
  permission: LoginReturnPermission
  @Field()
  result: LoginReturnResult
}

@ObjectType()
export class UserStateReturn {
  @Field({nullable: true})
  AIdx?: number
  @Field({nullable: true})
  AName?: string
  @Field({nullable: true})
  Permsn?: string
  @Field({nullable: true})
  Company?: string
  @Field({nullable: true})
  GroupID?: string
  @Field({nullable: true})
  Cms?: number
  @Field({nullable: true})
  Rank?: String
}