import { Module } from '@nestjs/common';
import { AgentService } from './agent.service';
import { AgentResolver } from './agent.resolver';
import { KnexModule } from 'src/module/knex/knex.module';
import { GraphqlModule } from '../graphql/graphql.module';
import { CommonModule } from '../common/common.module';

@Module({
  imports: [KnexModule, GraphqlModule, CommonModule],
  providers: [AgentResolver, AgentService],
  exports: [AgentService]
})
export class AgentModule {}
