import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { imgMulterOptions } from './upload-file-option';
import { UploadFileController } from './upload-file.controller';
import { UploadFileService } from './upload-file.service';

@Module({
  imports: [MulterModule.register(imgMulterOptions)],
  controllers: [UploadFileController],
  providers: [UploadFileService]
})
export class UploadFileModule {}
