import { Controller, Get, HttpException, HttpStatus, Post, Req, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileFieldsInterceptor, FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { Request } from 'express';
import { UploadFileService } from './upload-file.service';

@Controller('upload-file')
export class UploadFileController {
  constructor(
    private readonly uploadFileService: UploadFileService,
  ) {}

  @Get('upload')
  @UseInterceptors(FileInterceptor('file', {
    dest: 'temp/'
  }))
  getUploadFile() {
    return "hey";
  }
  
  @Post('upload')
  @UseInterceptors(FilesInterceptor('files', null))
  uploadFile(@UploadedFiles() files: Array<Express.Multer.File>) {
    const uploadedFiles: string[] = this.uploadFileService.uploadFiles(files);
    console.log(uploadedFiles)
  }

  @Post('upload-multiple')
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'avatar', maxCount: 3 },
    { name: 'background', maxCount: 3 },
  ]))
  uploadFileMultiple(@UploadedFiles() files: { avatar?: Express.Multer.File[], background?: Express.Multer.File[] }, @Req() request: Request ) {
    if(request.body.fileValidationError){
      throw new HttpException(`[${request.body.fileValidationError}] 이하의 파일들이 업로드에 실패하였습니다.`, HttpStatus.BAD_REQUEST);
    }
  }
}
