import { HttpException, HttpStatus } from "@nestjs/common";
import { MulterOptions } from "@nestjs/platform-express/multer/interfaces/multer-options.interface";
import { existsSync, mkdirSync } from "fs";
import { diskStorage } from "multer";
export const imgMulterOptions:MulterOptions = {
  fileFilter: (request, file, callback) => {
    if (file.mimetype.match(/\/(jpg|jpeg|png)$/)) {
      // 이미지 형식은 jpg, jpeg, png만 허용합니다.
      const uploadPath: string = 'temp';
      if (existsSync(`${uploadPath}/${file.originalname}`)) {
        request.body.fileValidationError = 
          request.body.fileValidationError ? 
            `${request.body.fileValidationError}, ${file.originalname}` 
              : 
            file.originalname;
        callback(null, false);
      }else{
        callback(null, true);
      }
    } else {
      callback(new HttpException('지원하지 않는 이미지 형식입니다.', HttpStatus.BAD_REQUEST), false);
    }
  },

  storage: diskStorage({
    destination: (request, file, callback) => {
      const uploadPath: string = 'temp';

      if (!existsSync(uploadPath)) {
        // public 폴더가 존재하지 않을시, 생성합니다.
        mkdirSync(uploadPath);
      }

      callback(null, uploadPath);
    },

    filename: function (req, file, cb) {
      // const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
      cb(null, file.originalname);
    }
  })
}

export const createImageURL = (file): string => {
  const serverAddress: string = "https://servertest.miraebiz.co.kr"
  
  // 파일이 저장되는 경로: 서버주소/public 폴더
  // 위의 조건에 따라 파일의 경로를 생성해줍니다.
  return `${serverAddress}/public/${file.filename}`;
}
