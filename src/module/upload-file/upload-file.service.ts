import { Injectable } from "@nestjs/common";
import { createImageURL, imgMulterOptions } from "./upload-file-option";

@Injectable()
export class UploadFileService {
  public imgMulterOptions = imgMulterOptions;

  uploadFiles(files: Array<Express.Multer.File>): string[] {
    const generatedFiles: string[] = [];

    for (const file of files) {
      generatedFiles.push(createImageURL(file));
      // http://localhost:8080/public/파일이름 형식으로 저장이 됩니다.
    }

    return generatedFiles;
  }
}