import { Controller, Get, Post, Req, Res, UploadedFile, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { ExcelService } from './excel.service';
import { Workbook } from 'exceljs';

@Controller('excel')
export class ExcelController {
  constructor(
    private excelService: ExcelService
  ){}

  @Post('readExcel')
  @UseInterceptors(FileInterceptor('excelFile'))
  readExcel(@UploadedFile() excelFile){
    const wb = new Workbook();
    if(excelFile.buffer){
      wb.xlsx.load(excelFile.buffer).then(workbook => {
        // console.log(workbook, 'workbook instance');
        workbook.eachSheet((sheet, id) => {
          sheet.eachRow((row, rowIndex) => {
            Array.isArray(row.values) && row.values.forEach((item, index) => console.log(item, index));
            // console.log(row.values, rowIndex);
          });
        })
      })
    }
  }

  @Post('multipleExcel')
  @UseInterceptors(FilesInterceptor('excelFiles'))
  multipleExcel(@UploadedFiles() excelFiles){
    console.log(excelFiles);
    return;
    const wb = new Workbook();
    if(excelFiles.buffer){
      wb.xlsx.load(excelFiles.buffer).then(workbook => {
        // console.log(workbook, 'workbook instance');
        workbook.eachSheet((sheet, id) => {
          sheet.eachRow((row, rowIndex) => {
            Array.isArray(row.values) && row.values.forEach((item, index) => console.log(item, index));
            // console.log(row.values, rowIndex);
          });
        })
      })
    }
  }

  @Get('tempExcel')
  tempExcelGet(@Req() req, @Res() res){
    this.excelService.tempExcel(req, res);
  }
}
