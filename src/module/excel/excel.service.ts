import { Injectable } from '@nestjs/common';
import { Workbook } from 'exceljs';
import {Request, Response} from 'express';

@Injectable()
export class ExcelService {
  tempExcel = async (req:Request, res:Response) => {
    var options = {
        filename: './streamed-workbook.xlsx',
        useStyles: true,
        useSharedStrings: true
    };
  
    var workbook = new Workbook();
    var sheet = workbook.addWorksheet('My Sheet');
    sheet.columns = [
        { header: 'Id', key: 'id', width: 10 },
        { header: 'Name', key: 'name', width: 32 },
        { header: 'D.O.B.', key: 'DOB', width: 10 }
    ];
  
    const Data = [
      {id: 1, name: 'John Doe', DOB: new Date().toUTCString()},
      {id: 2, name: 'John Doe', DOB: new Date().toLocaleDateString()},
      {id: 3, name: 'John Doe', DOB: new Date().toUTCString()},
      {id: 4, name: 'John Doe', DOB: new Date().toLocaleDateString()}
    ];
    sheet.addRows(Data);

    [
      {columns: ["A","C"], rows: ["1"]},
      {columns: ["B"], rows: ["1", "3"]}
    ].forEach(item => {
      item.columns.forEach(column => {
        item.rows.forEach(row => {
          sheet.getCell(`${column}${row}`).fill = {
            type: 'pattern',
            pattern:'solid',
            fgColor:{argb:'F08080'},
          };
        })
      })
    });
  
    const buffer = await workbook.xlsx.writeBuffer();
  
    res.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    res.setHeader("Content-Disposition", "attachment; filename=test.xlsx");
    res.end(buffer);
    // res.status(200).json(req.body);
  };
}
