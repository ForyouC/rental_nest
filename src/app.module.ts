import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MulterModule } from '@nestjs/platform-express';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AgentModule } from './module/agent/agent.module';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from './guard/roles.guard';
import { UploadFileModule } from './module/upload-file/upload-file.module';
import { PartnerModule } from './module/partner/partner.module';
import { GraphqlModule } from './module/graphql/graphql.module';
import { KnexModule } from './module/knex/knex.module';
import { CateModule } from './module/cate/cate.module';
import { ExcelModule } from './module/excel/excel.module';
import { CommonModule } from './module/common/common.module';
@Module({
  imports: [
    ConfigModule.forRoot(),
    GraphQLModule.forRoot({
      debug: true,
      playground: true,
      cors: {
        origin: ['http://localhost:2001', 'https://servertest.miraebiz.co.kr', 'https://clienttest.miraebiz.co.kr'],
        credentials: true,
      },
      context: ({ req, res }) => ({ req, res }),
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      sortSchema: true,
    }),
    AgentModule,
    UploadFileModule,
    PartnerModule,
    GraphqlModule,
    KnexModule,
    CateModule,
    ExcelModule,
    CommonModule,
  ],
  controllers: [AppController],
  providers: [AppService, {
    provide: APP_GUARD,
    useClass: RolesGuard,
  }],
})
export class AppModule {}
