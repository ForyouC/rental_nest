module.exports = {
  apps: [
    {
      name: 'nest',
      script: './dist/main.js',
      instances: 4,
      exec_mode: 'cluster',
      env: {
        "PM2": "true",
      }
    }
  ]
}